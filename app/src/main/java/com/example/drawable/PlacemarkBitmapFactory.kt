package com.example.drawable

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.graphics.PointF
import android.graphics.PorterDuff
import android.graphics.PorterDuffXfermode
import android.graphics.Rect
import android.graphics.Typeface
import androidx.core.graphics.ColorUtils
import kotlin.math.abs

class PlacemarkBitmapFactoryV2 constructor(val context: Context) {

    val sequence = 111

    private val textSize = context.dpToPx(12 * MULTIPLE_QUALITY)
    private val roundRadius = context.dpToPx(3 * MULTIPLE_QUALITY)

    private val sequenceContainerWidth = context.dpToPx(24 * MULTIPLE_QUALITY)
    private val sequenceContainerHeight = context.dpToPx(24 * MULTIPLE_QUALITY)
    private val sequenceDrawColor = Color.GREEN

    private val arrowHeight = context.dpToPx(7 * MULTIPLE_QUALITY)

    fun create(marker: TaskMarker): Bitmap {
        val bitmap = Bitmap.createBitmap(sequenceContainerWidth, sequenceContainerHeight + arrowHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawSequence(canvas)
        drawArrow(canvas)
        drawSequenceText(canvas, sequence.toString())
        return bitmap
    }

    private fun drawSequenceText(canvas: Canvas, text: String) {

        val fontPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            color = Color.WHITE
            textSize = this@PlacemarkBitmapFactoryV2.textSize.toFloat()
            typeface = Typeface.DEFAULT_BOLD
        }

        val fontWidth = fontPaint.measureText(text)

        val x = (sequenceContainerWidth.toFloat() / 2) - (fontWidth / 2)
        val y = (sequenceContainerHeight.toFloat() / 2) + (textSize / 3)

        canvas.drawText(text, x, y, fontPaint)
    }

    private fun drawSequence(canvas: Canvas) {
        //draw sequence background
        val backgroundPaint = Paint()
        val rect = Rect()
        backgroundPaint.color = sequenceDrawColor
        rect.set(
            0,
            0,
            sequenceContainerWidth,
            sequenceContainerHeight
        )
        canvas.drawRect(rect, backgroundPaint)

//        draw Left Top corner rounds
        val leftTopRect = Rect()
        leftTopRect.set(0, 0, roundRadius, roundRadius)
        drawRect(canvas, leftTopRect, sequenceDrawColor, CornerType.LEFT_TOP)

//        draw Left Bot corner rounds
        val leftBotRect = Rect()
        leftBotRect.set(0, sequenceContainerHeight - roundRadius, roundRadius, sequenceContainerHeight)
        drawRect(canvas, leftBotRect, sequenceDrawColor, CornerType.LEFT_BOT)

//        draw Right Top corner rounds
//        val rightTopRect = Rect()
//        rightTopRect.set(sequenceContainerWidth - roundRadius, 0, sequenceContainerWidth, roundRadius)
//        drawRect(canvas, rightTopRect, sequenceDrawColor, CornerType.RIGHT_TOP)

//        draw Right Bot corner rounds
//        val rightBotRect = Rect()
//        rightBotRect.set(sequenceContainerWidth - roundRadius, sequenceContainerHeight - roundRadius, sequenceContainerWidth, sequenceContainerHeight)
//        drawRect(canvas, rightBotRect, sequenceDrawColor, CornerType.RIGHT_BOT)
    }

    private fun drawArrow(canvas: Canvas) {
        val path = Path()
        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.color = ColorUtils.blendARGB(sequenceDrawColor, Color.BLACK, 0.25f)
        val arrowTop = sequenceContainerWidth
        val arrowBot = canvas.height
        val arrowStart = roundRadius
        val arrowEnd = ((sequenceContainerWidth - roundRadius) / 2) + roundRadius
        path.apply {
            reset()
            moveTo(arrowStart.toFloat(), arrowTop.toFloat())

            lineTo(arrowEnd.toFloat(), arrowBot.toFloat())

            lineTo(arrowEnd.toFloat(), arrowTop.toFloat())
            close()
        }

        canvas.drawPath(path, paint)
    }

    private fun drawRect(
        canvas: Canvas,
        point: Rect,
        color: Int,
        cornerType: CornerType
    ) {
        val paint = Paint()
        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)
        canvas.drawRect(point, paint)


        val radius = abs(point.right - point.left).toFloat()
        if (cornerType == CornerType.LEFT_TOP) {

            val cx = point.right.toFloat()
            val cy = point.bottom.toFloat()

            val backgroundPaint = Paint()
            backgroundPaint.color = color

            canvas.drawCircle(cx, cy, radius, backgroundPaint)
        }
        if (cornerType == CornerType.RIGHT_TOP) {
            val cx = (point.right - radius)
            val cy = point.bottom.toFloat()

            val backgroundPaint = Paint()
            backgroundPaint.color = color

            canvas.drawCircle(cx, cy, radius, backgroundPaint)
        }
        if (cornerType == CornerType.LEFT_BOT) {

            val cx = point.right.toFloat()
            val cy = (point.bottom - radius)

            val backgroundPaint = Paint()
            backgroundPaint.color = color

            canvas.drawCircle(cx, cy, radius, backgroundPaint)
        }
        if (cornerType == CornerType.RIGHT_BOT) {

            val cx = (point.right - radius)
            val cy = (point.bottom - radius)

            val backgroundPaint = Paint()
            backgroundPaint.color = color

            canvas.drawCircle(cx, cy, radius, backgroundPaint)
        }
    }

    fun getAnchorPoint(placemarkBitmap: Bitmap) = PointF(
        0F,
        0F
    )

    enum class CornerType {
        LEFT_TOP, RIGHT_TOP, LEFT_BOT, RIGHT_BOT
    }

    companion object {
        const val MULTIPLE_QUALITY = 5
    }
}