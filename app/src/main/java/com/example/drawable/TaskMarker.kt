package com.example.drawable

data class TaskMarker(
    val sequence: Int
)