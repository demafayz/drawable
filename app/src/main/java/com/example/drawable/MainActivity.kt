package com.example.drawable

import android.content.Context
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import com.example.drawable.ui.theme.DrawableTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            DrawableTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Greeting(this, "Android")
                }
            }
        }
    }
}

@Composable
fun Greeting(context: Context, name: String, modifier: Modifier = Modifier) {
    val bitmapFactory = PlacemarkBitmapFactoryV2(context = context)
    val marker = TaskMarker(1)
    val bitmap = bitmapFactory.create(marker).asImageBitmap()
    Image(bitmap = bitmap, contentDescription = "")
}

//@Preview(showBackground = true)
//@Composable
//fun GreetingPreview() {
//    DrawableTheme {
//        Greeting(, "Android")
//    }
//}